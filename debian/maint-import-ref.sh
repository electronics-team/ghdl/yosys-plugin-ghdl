#!/bin/sh
# Upstream doesn't provide git tags or tarballs, so make up commit date
# version numbers.
set -xe
ref=${1:-upstream}
git fetch https://github.com/ghdl/ghdl-yosys-plugin.git master:upstream

uver=$(git show "$ref" -q --format=0.0~git%cd.%h --date=format:%Y%m%d)
mangled_uver=$(git show "$ref" -q --format=0.0_git%cd.%h --date=format:%Y%m%d)

git tag -f "upstream/$mangled_uver" "$ref"

latest_tag=$(git describe --abbrev=0 --tags "$ref")
[ $latest_tag != upstream/$mangled_uver ] || return

#^ gbp-import-ref fails to create the tag ffs.
dch --newversion 0.0~git20230419.5b64ccf-1 New upstream release.
#^ gbp dch is also broken :/
gbp import-ref "$ref" -u"$uver"
